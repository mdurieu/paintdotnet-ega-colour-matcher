// Name:
// Submenu:
// Author:
// Title:
// Version:
// Desc:
// Keywords:
// URL:
// Help:

private ColorBgra[] colors;
private List<DitheredColor> ditheredColors;

void initColors(){
        colors = new ColorBgra[16];
        colors[0] = ColorBgra.FromBgraClamped(0, 0, 0, 255); //000000   Black
        colors[1] = ColorBgra.FromBgraClamped(170, 0, 0, 255); //0000AA  Blue
        colors[2] = ColorBgra.FromBgraClamped(0, 170, 0, 255); //00AA00  Green
        colors[3] = ColorBgra.FromBgraClamped(170, 170, 0, 255); //00AAAA Cyan
        colors[4] = ColorBgra.FromBgraClamped(0, 0, 170, 255); //AA0000  Red
        colors[5] = ColorBgra.FromBgraClamped(170, 0, 170, 255); //AA00AA Magenta
        colors[6] = ColorBgra.FromBgraClamped(0, 85, 170, 255); //AA5500  Brown
        colors[7] = ColorBgra.FromBgraClamped(170, 170, 170, 255); //AAAAAA Light grey
        colors[8] = ColorBgra.FromBgraClamped(85, 85, 85, 255); //555555 Dark grey
        colors[9] = ColorBgra.FromBgraClamped(255, 85, 85, 255); //5555FF Bright Blue
        colors[10] = ColorBgra.FromBgraClamped(85, 255, 85, 255); //55FF55 Bright Green
        colors[11] = ColorBgra.FromBgraClamped(255, 255, 85, 255); //55FFFF Bright Cyan
        colors[12] = ColorBgra.FromBgraClamped(85, 85, 255, 255); //FF5555 Bright Red
        colors[13] = ColorBgra.FromBgraClamped(255, 85, 255, 255); //FF55FF Bright Magenta
        colors[14] = ColorBgra.FromBgraClamped(85, 255, 255, 255); //FFFF55 Bright Yellow
        colors[15] = ColorBgra.FromBgraClamped(255, 255, 255, 255); //FFFFFF White

        ditheredColors = new List<DitheredColor>();

        for(int i = 0; i < colors.Length; i++){
            for(int j = 0; j < colors.Length; j++){
                DitheredColor newColor = new DitheredColor(colors[i], colors[j]);
                if(!DoesDitheredColorExist(newColor) && !IsDitheredColorABaseColor(newColor)){
                     ditheredColors.Add(newColor);
                }          
               
            }
        }

       
}

//Used when generating dithered colours, check if the colour we've made already exists as one of the base colours
//This can occur when dithering between primary colours and greys etc
bool IsDitheredColorABaseColor(DitheredColor newColor){
    //If both colours of the dithered colour are the same, return it as this will allow the base colour to be matched properly
    if(newColor.color1.R == newColor.color2.R && newColor.color1.G == newColor.color2.G && newColor.color1.B == newColor.color2.B) return false;

     for(int i = 0; i < colors.Length; i++){
        if(colors[i].R == newColor.ditheredColor.R && colors[i].G == newColor.ditheredColor.G && colors[i].B == newColor.ditheredColor.B) return true;
    }

    return false;
}
//Used when generating the list of dithered colours to prevent duplicates, ie if creating a dithered colour between
//black and blue, we don't need to also create one when we come to assess blue and black
bool DoesDitheredColorExist(DitheredColor newColor){
    for(int i = 0; i < ditheredColors.Count; i++){
        if(ditheredColors[i].ditheredColor.R == newColor.ditheredColor.R && ditheredColors[i].ditheredColor.G == newColor.ditheredColor.G && ditheredColors[i].ditheredColor.B == newColor.ditheredColor.B) return true;
    }

    return false;
}

class DitheredColor{
    public ColorBgra color1;
    public ColorBgra color2;
    public ColorBgra ditheredColor;

    public DitheredColor(ColorBgra color1, ColorBgra color2){
        this.color1 = color1;
        this.color2 = color2;



        int ditheredR = (int)((color1.R + color2.R) / 2);
        int ditheredG = (int)((color1.G + color2.G) / 2);
        int ditheredB = (int)((color1.B + color2.B) / 2);

        ditheredColor = ColorBgra.FromBgraClamped(ditheredB, ditheredG, ditheredR, 255);
    }
}

void Render(Surface dst, Surface src, Rectangle rect)
{
       if(colors == null){
           initColors();
       } 

    ColorBgra currentPixel;
    for (int y = rect.Top; y < rect.Bottom; y++)
    {
        if (IsCancelRequested) return;
        for (int x = rect.Left; x < rect.Right; x++)
        {
            currentPixel = src[x,y];

            if(currentPixel.R == 0 && currentPixel.G == 0 && currentPixel.B == 0 || currentPixel.A == 0) continue;

            DitheredColor matchedColor = matchColor(currentPixel);

            if(x % 2 == 0){
                if(y % 2 == 0){
                    dst[x,y] = matchedColor.color1;
                } else {
                    dst[x,y] = matchedColor.color2;
                }
            } else {
                if(y % 2 == 0){
                    dst[x,y] = matchedColor.color2;
                } else {
                    dst[x,y] = matchedColor.color1;
                }
            }


            
        }
    }
}

private DitheredColor matchColor(ColorBgra inputColor){
    DitheredColor matchedColor = ditheredColors[0];

    float closestDistance = 9999999;
    for (int k = 0; k < ditheredColors.Count; k++)
    {
        var referenceLAB = rgbToLab(inputColor);
        var comparisonLab = rgbToLab(ColorBgra.FromBgraClamped(ditheredColors[k].ditheredColor.B, ditheredColors[k].ditheredColor.G, ditheredColors[k].ditheredColor.R, 255));

        var deltaL = referenceLAB[0] - comparisonLab[0];
        var deltaA = referenceLAB[1] - comparisonLab[1];
        var deltaB = referenceLAB[2] - comparisonLab[2];

        if(inputColor.R == 85 && inputColor.G == 85 && inputColor.B == 85){
           if(ditheredColors[k].ditheredColor.B == 85 && ditheredColors[k].ditheredColor.G == 85 && ditheredColors[k].ditheredColor.R == 85){
           //    MessageBox.Show(deltaL + ", " + deltaA + ", " + deltaB);
            }
        }

        float deltaE = (float)Math.Sqrt(Math.Pow(deltaL, 2f) + Math.Pow(deltaA, 2f) + Math.Pow(deltaB, 2f));
        if (deltaE < closestDistance)
        {
           //  MessageBox.Show("DeltaE now: " + deltaE + " matched color: " + ditheredColors[k].ditheredColor.ToString());
            closestDistance = deltaE;            
            matchedColor = ditheredColors[k];
        }
    }

    return matchedColor;
}

private float[] rgbToLab(ColorBgra inputColor)
    {
        float[] RGB = new float[] { 0, 0, 0 };

        for (int i = 0; i < 3; i++)
        {
            float value = 0;
            switch (i)
            {
                case 0:
                    value = inputColor.R;
                    break;

                case 1:
                    value = inputColor.G;
                    break;

                case 2:
                    value = inputColor.B;
                    break;

            }

            if (value > 0.0405)
            {
                value = (float)Math.Pow((value + 0.055) / 1.055, 2.4);
            } else
            {
                value = value / 12.92f;
            }

            RGB[i] = value * 100f;
        }

        float[] XYZ = new float[] { 0, 0, 0 };


        var X = RGB[0] * 0.4124 + RGB[1] * 0.3576 + RGB[2] * 0.1805;
        var Y = RGB[0] * 0.2126 + RGB[1] * 0.7152 + RGB[2] * 0.0722;
        var Z = RGB[0] * 0.0193 + RGB[1] * 0.1192 + RGB[2] * 0.9505;
        XYZ[0] = (float)Math.Round(X, 4);
        XYZ[1] = (float)Math.Round(Y, 4);
        XYZ[2] = (float)Math.Round(Z, 4);

        XYZ[0] = XYZ[0] / 95.047f;
        XYZ[1] = XYZ[1] / 100.0f;
        XYZ[2] = XYZ[2] / 108.883f;

        for(int i = 0; i < 3; i++)
        {
            if(XYZ[i] > 0.008856)
            {
                XYZ[i] = (float)Math.Pow(XYZ[i], 0.3333333333333333);
            } else
            {
                XYZ[i] = (7.787f * XYZ[i]) + (16f / 116f);
            }
        }



        float[] LAB = new float[] { 0, 0, 0 };

        float L = (116 * XYZ[1]) - 16;
        float a = 500 * (XYZ[0] - XYZ[1]);
        float b = 200 * (XYZ[1] - XYZ[2]);

        LAB[0] = (float)Math.Round(L, 4);
        LAB[1] = (float)Math.Round(a, 4);
        LAB[2] = (float)Math.Round(b, 4);

        return LAB;
    }
